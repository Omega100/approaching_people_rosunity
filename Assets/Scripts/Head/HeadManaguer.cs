﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HeadManaguer : MonoBehaviour {

    public GameObject _texBox;

    public float _defaultTimeMsg = 3;

    public void Start()
    {
        HideTextBox();
    }

    public void Said(string msg)
    {
        Said(msg, _defaultTimeMsg);
    }

    public void Said(string msg, float time) {
        _texBox.GetComponentInChildren<Text>().text = msg;
        _texBox.SetActive(true);
        Invoke("HideTextBox", time);
    }

    public void HideTextBox() {
        _texBox.SetActive(false);
    }
}
